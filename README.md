[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1ZKkxUXcSDQFc4PEee6rpSsvkSuH4CMt1?usp=sharing)

# Knight’s Tour Problem: Genetic Algorithms with Heuristic

Knights Tour - Genetic Algorithm Implementation based on work proposed by Jafar Al-Gharaibeh, Zakariya Qawagneh and Hiba Al-Zahawi, with some minor modifications.

# Resources
Al-Gharaibeh, J., Qawagneh, Z. and Al-Zahawi, H., 2007. Genetic Algorithms With Heuristic - Knight's Tour Problem.. [online] Available at <https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.115.3709&rep=rep1&type=pdf> [Accessed 11 December 2020].